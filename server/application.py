import gevent
from neutral_player import login_neutral, run_neutral
import json
import command_actions
import info_actions
import network
import game
from gevent.wsgi import WSGIServer
from flask import Flask, request, redirect

app = Flask(__name__, static_folder = '../static')
app.config.from_object('settings')

def game_timer():
    neutrals = []
    for i in range(app.config['NEUTRAL_PLAYERS']):
        neutrals.append(login_neutral(i))
    
    gevent.sleep(5)

    while game.time_remaining() > 0:
        for player in neutrals:
            run_neutral(player)
        network.transfer_packets()
        game.decrement_time()
        gevent.sleep(1)

    print 'Game Over'

@app.route('/')
def home():
    return redirect('/static/ControlPanel.html')

@app.route('/command', methods=['POST'])
def command():
    res = command_actions.ROUTES[request.form['action']](request.form)
    return json.dumps(res)

@app.route('/info', methods=['GET'])
def info():
    res = info_actions.ROUTES[request.args['action']](request.args)
    return json.dumps(res)

if __name__ == '__main__':
    print "Running server on port 8080"
    gevent.spawn(game_timer)
    WSGIServer(('', 8080), app).serve_forever()
