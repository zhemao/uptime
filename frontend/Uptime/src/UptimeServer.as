package  
{
	import org.flixel.*;
	
	public class UptimeServer extends FlxState
	{

		private var title:FlxText;
		private var server:FlxText;
		//private var server:FlxText;
		private var serverField:FlxInput;
		//private var serverField:FlxInput;
		private var serverButton:FlxButton;

		public function UptimeServer() 
		{
			title = new FlxText(0, 60, 640, "Uptime", false);
			title.alignment = "center";
			title.size = 32;

			
			server = new FlxText(0, 160, 120, "Server", false);
			//server = new FlxText(0, 110, 160, "Server Address", false);
			server.alignment = "right";
			//server.alignment = "right";
			
			serverField = new FlxInput(124, 160, 500, false);
			//serverField = new FlxInput(FlxG.width / 2 + 2, 110, 100, "IP", false);
			serverButton = new FlxButton(240, 280, "Join Server", doserver);
			
			
			
			add(title);
			add(server);
			//add(server);
			add(serverField);
			add(serverButton);
			
			FlxG.mouse.show();
		}
		
		override public function update():void
		{
			super.update();
			if (FlxG.keys.justPressed("ENTER"))
			{
				doserver();
			}
		}
		
		public function doserver():void
		{
			Globals.server = serverField.input.text
			FlxG.switchState(new UptimeGame());
			//trace("Done");
		}
		
	}

}