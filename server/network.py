import game
import gevent
import random
from game import PlayerServers, NetworkRequests

def gen_transfer_queue():
    transq = []
    for sender in NetworkRequests:
        for receiver in NetworkRequests[sender]:
            transq.append(NetworkRequests[sender][receiver].copy())
    random.shuffle(transq)
    return transq

def check_firewall(rules, ip):
    for rule in rules:
        if ip >= rule.min and ip <= rule.max:
            return False
    return True

def send_one_packet(transfer):
    sender = PlayerServers[transfer.sender]
    receiver = PlayerServers[transfer.receiver]

    if sender.traffic >= sender.bandwidth or \
            receiver.traffic >= receiver.bandwidth or \
            receiver.ip != transfer.ip:
        transfer.requested = 0
        return

    if check_firewall(receiver.rules, sender.ip):
        sender.traffic += 1
        receiver.traffic += 1
        if sender.team == receiver.team or sender.team == -1 or receiver.team == -1:
            sender.uptime += 1
            receiver.uptime += 1
        transfer.requested -= 1
        transfer.actual += 1
    else:
        transfer.requested = 0
        transfer.actual = 0
    

def transfer_packets():
    for player in PlayerServers.values():
        player.traffic = 0

    transq = gen_transfer_queue()

    while len(transq) > 0:
        transfer = transq.pop(0)
        if transfer.requested > 0:
            send_one_packet(transfer)
            transq.append(transfer)
        else:
            NetworkRequests[transfer.sender][transfer.receiver].actual = transfer.actual

def network_schedule():
    while True:
        transfer_packets()
        gevent.sleep(1)
