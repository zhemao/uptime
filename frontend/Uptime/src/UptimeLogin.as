package  
{
	import flash.text.TextField;
	import org.flixel.*;
	
	public class UptimeLogin extends FlxState
	{

		private var title:FlxText;
		private var login:FlxText;
		//private var server:FlxText;
		private var loginField:FlxInput;
		//private var serverField:FlxInput;
		private var loginButton:FlxButton;

		public function UptimeLogin() 
		{
			title = new FlxText(0, 60, 640, "Uptime", false);
			title.alignment = "center";
			title.size = 32;

			
			login = new FlxText(0, 160, 120, "Login", false);
			//server = new FlxText(0, 110, 160, "Server Address", false);
			login.alignment = "right";
			//server.alignment = "right";
			
			loginField = new FlxInput(124, 160, 500, false);
			//serverField = new FlxInput(FlxG.width / 2 + 2, 110, 100, "IP", false);
			loginButton = new FlxButton(240, 280, "Login", logger);
			
			
			
			add(title);
			add(login);
			//add(server);
			add(loginField);
			add(loginButton);
			
			FlxG.mouse.show();
		}
		
		override public function update():void
		{
			super.update();
			if (FlxG.keys.justPressed("ENTER"))
			{
				logger();
			}
		}
		
		public function logger()
		{
			Globals.name = loginField.input.text
			FlxG.switchState(new UptimeServer());
		}
		
	}

}