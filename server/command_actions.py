from game import PlayerServers, NetworkRequests, LastTeam
from game import Player, Rule, NetTransfer
from game import ip_valid, find_player_id, find_player_ip, time_remaining
from settings import IPMAX
import random
import uuid

def sendpackets(req):
    token = str(req['token'])
    ip = int(req['ip'])
    num = int(req['num'])

    if time_remaining() <= 0:
        return {"status": "gameover"}

    fromserv = PlayerServers.get(token)
    if not fromserv:
        return {"status": "badtoken"}

    toserv = find_player_ip(ip)
    if not toserv:
        return {"status": "noip"}

    if num < 0:
        return {"status": "badcommand"}

    NetworkRequests[fromserv.token][toserv.token] = NetTransfer(fromserv.token,
                                                                toserv.token,
                                                                ip, num)

    return {"status": "ok"}

def addrule(req):
    token = str(req['token'])
    fwmin = int(req['min'])
    fwmax = int(req['max'])
    
    if time_remaining() <= 0:
        return {"status": "gameover"}

    serv = PlayerServers.get(token)
    if not serv:
        return {"status": "badtoken"}

    if not ip_valid(fwmin) or not ip_valid(fwmax):
        return {"status": "badcommand"}
    
    rule = Rule(fwmin, fwmax)
    
    serv.rules.append(rule)

    return {"status": "ok"}

def delrule(req):
    token = str(req['token'])
    ruleid = int(req['ruleid'])

    if time_remaining() <= 0:
        return {"status": "gameover"}

    serv = PlayerServers.get(token)
    if not serv:
        return {"status": "badtoken"}
    
    if ruleid < 0 or ruleid >= len(serv.rules):
        return {"status": "badcommand"}
    
    del serv.rules[ruleid]

    return {"status": "ok"}

def changeip(req):
    ip = int(req['ip'])
    token = str(req['token'])

    if time_remaining() <= 0:
        return {"status": "gameover"}

    if not ip_valid(ip):
        return {"status": "badcommand"}

    if find_player_ip(ip):
        return {"status": "iptaken"}
    
    serv = PlayerServers.get(token)
    if not serv:
        return {"status": "badtoken"}

    serv.ip = ip
    return {"status": "ok", "ip": ip}

def login(req):
    global LastTeam
    playerid = str(req['playerid'])
    
    if time_remaining() <= 0:
        return {"status": "gameover"}

    if find_player_id(playerid):
        return {"status": "idexists"}
    
    token = uuid.uuid4().hex
    ip = random.randint(0, IPMAX)

    while find_player_ip(ip):
        ip = random.randint(0, IPMAX)

    player = Player(playerid, token, ip, LastTeam)
    PlayerServers[token] = player

    retval = {"status": "ok", "ip": ip, "token": token, "team": LastTeam}
    
    LastTeam = 1 if LastTeam == 0 else 0

    return retval

ROUTES = {
    "send": sendpackets,
    "addrule": addrule,
    "delrule": delrule,
    "changeip": changeip,
    "login": login
}
