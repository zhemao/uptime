from collections import defaultdict, namedtuple
from settings import IPMAX, GAMETIME

NetworkRequests = defaultdict(dict)
PlayerServers = {}
LastTeam = 0
Rule = namedtuple('Rule', ['min', 'max'])
GameTimeRemaining = GAMETIME

class NetTransfer(object):
    def __init__(self, sender, receiver, ip, num):
        self.sender = sender
        self.receiver = receiver
        self.ip = ip
        self.requested = num
        self.actual = 0

    def __repr__(self):
        return ('NetTransfer(sender=' + self.sender + ', receiver=' +
                self.receiver + ', ip=' + str(self.ip) + ', requested=' +
                str(self.requested) + ', actual=' + str(self.actual) + ')')

    def copy(self):
        return NetTransfer(self.sender, self.receiver, self.ip, self.requested)

class Player(object):
    def __init__(self, playerid, token, ip, team):
        self.playerid = playerid
        self.token = token
        self.ip = ip
        self.team = team
        self.rules = []
        self.uptime = 0
        self.bandwidth = 100
        self.traffic = 0

def find_player_ip(ip):
    for player in PlayerServers.values():
        if player.ip == ip:
            return player
    return None

def find_player_id(playerid):
    for player in PlayerServers.values():
        if player.playerid == playerid:
            return player
    return None

def ip_valid(ip):
    return ip >= 0 and ip < IPMAX

def time_remaining():
    return GameTimeRemaining

def decrement_time():
    global GameTimeRemaining
    GameTimeRemaining -= 1
