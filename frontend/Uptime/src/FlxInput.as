package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Michael Tango
	 */
	public class FlxInput extends FlxGroup
	{
		public var input:FlxText;
		private var box:FlxSprite;
		private var alphabet:Array = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
		public function FlxInput(x:int, y:int, width:int, font:Boolean) 
		{
			input = new FlxText(x + 2, y, width - 2, "", font);
			box = new FlxSprite(x, y);
			box.makeGraphic(width, 20, 0xff888888);
			trace(alphabet);
			//trace("Test");
			this.add(box);
			this.add(input);
			
		}
		
		override public function update():void
		{
			for (var letter:String in alphabet)
			{
				if (FlxG.keys.justPressed(alphabet[letter]))
				{
					input.text = input.text + alphabet[letter];
				}
				
			}
			
			if (FlxG.keys.justPressed("BACKSPACE"))
			{
				input.text = input.text.substr(0, input.text.length - 1);
			}
		}
		
	}

}