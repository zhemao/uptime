from game import PlayerServers, NetworkRequests
from game import ip_valid, find_player_id, find_player_ip, time_remaining
from collections import defaultdict

def get_uptimes(req):
    uptimes = []
    for player in PlayerServers.values():
        utobj = {
            "playerid": player.playerid,
            "team": player.team,
            "uptime": player.uptime
        }
        uptimes.append(utobj)
    return {"status": "ok", "uptimes": uptimes}

class NetInfo(object):
    def __init__(self):
        self.inbound = 0
        self.outbound = 0
        self.playerid = ""
        self.ip = -1
        self.team = -1

    def to_dict(self):
        return {'inbound': self.inbound,
                'outbound': self.outbound,
                'playerid': self.playerid,
                'ip': self.ip,
                'team': self.team}

def get_network_info(req):
    token = str(req['token'])
    transfers = defaultdict(NetInfo)

    for sender in NetworkRequests:
        if sender != token and token in NetworkRequests[sender]:
            transfers[sender].playerid = PlayerServers[sender].playerid
            transfers[sender].ip = PlayerServers[sender].ip
            transfers[sender].team = PlayerServers[sender].team
            transfers[sender].inbound = NetworkRequests[sender][token].actual
    
    for receiver in NetworkRequests[token]:
        transfers[receiver].playerid = PlayerServers[receiver].playerid
        transfers[receiver].ip = PlayerServers[receiver].ip
        transfers[receiver].team = PlayerServers[receiver].team
        transfers[receiver].outbound = NetworkRequests[token][receiver].actual

    return {'status': 'ok',
            'bandwidth': PlayerServers[token].bandwidth, 
            'traffic': [netinfo.to_dict() for netinfo in transfers.values()]}

def list_firewall_rules(req):
    token = str(req['token'])

    rules = [{'min': rule.min, 'max': rule.max} 
                for rule in PlayerServers[token].rules]

    return {'status': 'ok', 'rules': rules}

def show_time_remaining(req):
    return {'status': 'ok', 'time': time_remaining()}

ROUTES = {
    'uptimes': get_uptimes,
    'network': get_network_info,
    'firewall': list_firewall_rules,
    'gametime': show_time_remaining
}
