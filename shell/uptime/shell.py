from .commands import Uptime, UptimeException
import sys

def run_command(ut, comm):
    args = comm.split(' ')

    if args[0] == 'login':
        if len(args) < 2:
            raise UptimeException('badargs')
        resp = ut.login(args[1])
        print "IP: " + str(resp['ip'])
        print "Team: " + str(resp['team'])
    elif args[0] == 'send':
        if len(args) < 3:
            raise UptimeException('badargs')
        ut.send(args[1], args[2])
    elif args[0] == 'addrule':
        if len(args) < 3:
            raise UptimeException('badargs')
        ut.addrule(args[1], args[2])
    elif args[0] == 'delrule':
        if len(args) < 2:
            raise UptimeException('badargs')
        ut.delrule(args[1])
    elif args[0] == 'changeip':
        if len(args) < 2:
            raise UptimeException('badargs')
        ut.changeip(args[1])
    elif args[0] == 'firewall':
        resp = ut.firewall()
        for i, rule in enumerate(resp['rules']):
            print str(i) + ' ' + str(rule['min']) + ' - ' + str(rule['max'])
    elif args[0] == 'uptimes':
        resp = ut.uptimes()
        for uptime in resp['uptimes']:
            print uptime['playerid'] + ' ' + str(uptime['team']) + ' ' + str(uptime['uptime'])
    elif args[0] == 'network':
        resp = ut.network()
        print 'Total bandwidth: ' + str(resp['bandwidth'])
        for transfer in resp['traffic']:
            print transfer['playerid'] + ' ' + str(transfer['team']) + ' ' + str(transfer['ip']) + ' ' + str(transfer['inbound']) + ' ' + str(transfer['outbound'])
    elif args[0] == 'gametime':
        resp = ut.gametime()
        print resp['time']
    else:
        raise UptimeException('badcommand')


def run_shell(addr):
    ut = Uptime(addr)
    comm = raw_input('uptime> ')

    while comm != 'exit':
        try:
            run_command(ut, comm)
        except UptimeException as e:
            print e
        comm = raw_input('uptime> ')

def main():
    if len(sys.argv) == 1:
        print "Usage: " + sys.argv[0] + " address"
        sys.exit(1)

    run_shell(sys.argv[1])
