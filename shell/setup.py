from setuptools import setup

setup(
    name = 'uptime',
    version = '0.2',
    description = 'The Game of Network Warfare',
    author = 'Zhehao Mao',
    author_email = 'zhehao.mao@gmail.com',
    packages = ['uptime'],
    install_requires = ['requests'],
    entry_points = {
        'console_scripts': [
            'utsh = uptime.shell:main'
        ]
    }
)
