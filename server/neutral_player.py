from game import NetworkRequests, PlayerServers
from game import find_player_ip, Player, NetTransfer
from settings import IPMAX, IPCHANGEP, NEUTRAL_PLAYERS, NEUTRAL_TRAFFIC_MAX
import uuid
import random
import gevent

def random_ip():
    ip = random.randint(0, IPMAX)
    while find_player_ip(ip):
        ip = random.randint(0, IPMAX)
    return ip

def login_neutral(rank):
    token = uuid.uuid4().hex
    
    ip = random_ip()

    player = Player('neutral' + str(rank), token, ip, -1)
    PlayerServers[token] = player

    return player

def run_neutral(neutral):
    if random.random() < IPCHANGEP:
        neutral.ip = random_ip()
    for player in PlayerServers.values():
        if player.team != -1:
            sender = neutral.token
            receiver = player.token
            num = random.randint(0, NEUTRAL_TRAFFIC_MAX)
            NetworkRequests[sender][receiver] = NetTransfer(sender, receiver, 
                                                            player.ip, num)

def neutral_loop():
    neutrals = []
    
    for i in range(NEUTRAL_PLAYERS):
        neutrals.append(login_neutral(i))
    
    while True:
        for i, neutral in enumerate(neutrals):
            run_neutral(neutral)
        gevent.sleep(1)
