import requests
import json
from collections import namedtuple

Rule = namedtuple('Rule', ['min', 'max'])

class UptimeException(BaseException):
    def __init__(self, status):
        self.status = status
    def __str__(self):
        return 'UptimeException: ' + self.status

def checkstatus(r):
    resp = json.loads(r.text)
    
    if resp['status'] != 'ok':
        raise UptimeException(resp['status'])

    return resp

class Uptime(object):
    def __init__(self, addr):
        self.token = None
        self.command_addr = addr + '/command'
        self.info_addr = addr + '/info'
        self.rules = []

    def send_command(self, action, **kwargs):
        kwargs['action'] = action
        if self.token:
            kwargs['token'] = self.token
        r = requests.post(self.command_addr, data = kwargs)
        resp = checkstatus(r)
        return resp

    def get_info(self, action, **kwargs):
        kwargs['action'] = action
        if self.token:
            kwargs['token'] = self.token
        r = requests.get(self.info_addr, params = kwargs)
        resp = checkstatus(r)
        return resp

    def login(self, playerid):
        self.playerid = playerid

        authinfo = self.send_command('login', playerid = playerid)

        self.token = authinfo['token']
        self.ip = authinfo['ip']
        self.team = authinfo['team']

        return authinfo

    def send(self, ip, num):
        self.send_command('send', ip = str(ip), num = str(num))

    def addrule(self, addrmin, addrmax):
        self.send_command('addrule', min = str(addrmin), max = str(addrmax))
        self.rules.append(Rule(int(addrmin), int(addrmax)))

    def delrule(self, index):
        self.send_command('delrule', ruleid = str(index))
        del self.rules[int(index)]
    
    def changeip(self, ip):
        self.send_command('changeip', ip = str(ip))
        self.ip = int(ip)

    def firewall(self):
        return self.get_info('firewall')

    def uptimes(self):
        return self.get_info('uptimes')

    def network(self):
        return self.get_info('network')

    def gametime(self):
        return self.get_info('gametime')
